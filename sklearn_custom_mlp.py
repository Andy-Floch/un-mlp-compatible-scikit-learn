from typing import List
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from sklearn.preprocessing import LabelBinarizer


def log_loss(A, y):
    return (1 / len(y)) * np.sum(-y * np.log(A) - (1 - y) * np.log(1 - A))


class CustomMLPClassifier(BaseEstimator, ClassifierMixin):

    """A custom MLP classifier
    Parameters
    ----------
    demo_param : str, default='demo'
        A parameter used for demonstation of how to pass and store paramters.
    Attributes
    ----------
    X_ : ndarray, shape (n_samples, n_features)
        The input passed during :meth:`fit`.
    y_ : ndarray, shape (n_samples,)
        The labels passed during :meth:`fit`.
    classes_ : ndarray, shape (n_classes,)
        The classes seen at :meth:`fit`.

    Pour passer à du multi class, eg iris
    N outputs pour N classes (3 pour iris)
    On vise des activations à 0 pour les mauvaises classes et 1 pour la bonne (exemple [0, 1, 0])
    On doit modifier la back prop pour gérer plusieurs outputs
    il faudra donc one-hot-encoder 'y' pour avoir un dZ de longueur N (d'où le label binarizer du MLP de sklearn)
    Et à la prédiction, on prend la plus grande des valeurs des activations
    Cf https://www.kaggle.com/code/vitorgamalemos/multilayer-perceptron-from-scratch/notebook#6.-Implementation-the-Multilayer-Perceptron-in-Python
    """  # noqa: E501

    coefs_: List

    def __init__(
        self, hidden_layers=[3, 3], learning_rate=0.1, n_iter=1000, random_state=None, verbose=False
    ):
        self.hidden_layers = hidden_layers
        self.learning_rate = learning_rate
        self.n_iter = n_iter
        self.random_state = random_state
        self.verbose = verbose

    def _initialize_weights_and_bias(self):
        if self.verbose:
            print("Initializing weights and biases")

        layers = [self.X_.shape[0], *self.hidden_layers, self.y_.shape[0]]
        self.coefs_ = []
        for i in range(1, len(layers)):
            self.coefs_.append(
                {
                    "W": np.random.randn(layers[i], layers[i - 1]),
                    "b": np.random.randn(layers[i], 1),
                }
            )

        if self.verbose > 1:
            for i, p in enumerate(self.coefs_, start=1):
                print(f"Param {i}")
                print(f"  -> Shape of W: {p['W'].shape}")
                print(f"  -> Shape of b: {p['b'].shape}")

    def _forward_propagation(self, X):
        Z = self.coefs_[0]["W"].dot(X) + self.coefs_[0]["b"]
        activations = [1 / (1 + np.exp(-Z))]
        for p in self.coefs_[1:]:
            Z = p["W"].dot(activations[-1]) + p["b"]
            activations.append(1 / (1 + np.exp(-Z)))
        return activations

    def _back_propagation(self, activations):
        # dW = (1 / len(y)) * np.dot(X.T, A - y)
        # db = (1 / len(y)) * np.sum(A - y)

        Ws = [p["W"] for p in self.coefs_]

        m = self.y_.shape[1]

        # Pour le multi class: ici self.y_ doit être encodé ce qui donne genre dZ = [0.3, 0.6, 0.86] - [0, 1, 0]  # noqa: E501
        dZ = activations[-1] - self.y_
        gradients = [
            {
                "dW": 1 / m * dZ.dot(activations[-2].T),
                "db": 1 / m * np.sum(dZ, axis=1, keepdims=True),
            }
        ]

        for i in range(len(activations) - 2, 0, -1):
            dZ = np.dot(Ws[i + 1].T, dZ) * activations[i] * (1 - activations[i])
            gradients.append(
                {
                    "dW": 1 / m * dZ.dot(activations[i].T),
                    "db": 1 / m * np.sum(dZ, axis=1, keepdims=True),
                }
            )

        dZ = np.dot(Ws[1].T, dZ) * activations[0] * (1 - activations[0])
        gradients.append(
            {
                "dW": 1 / m * dZ.dot(self.X_.T),
                "db": 1 / m * np.sum(dZ, axis=1, keepdims=True),
            }
        )

        gradients.reverse()
        return gradients

    def _update(self, gradients):
        # W = W - learning_rate * dW
        # b = b - learning_rate * db
        # return (W, b)

        new_params = []

        for g, p in zip(gradients, self.coefs_):
            new_params.append(
                {
                    "W": p["W"] - self.learning_rate * g["dW"],
                    "b": p["b"] - self.learning_rate * g["db"],
                }
            )

        self.coefs_ = new_params

    def fit(self, X, y, incremental=False, return_fit_report=False):
        """A reference implementation of a fitting function for a classifier.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.
        y : array-like, shape (n_samples,)
            The target values. An array of int.
        Returns
        -------
        self : object
            Returns self.
        """
        # Check that X and y have correct shape
        X, y = check_X_y(X, y, multi_output=True)
        # Store the classes seen during fit
        self.classes_ = unique_labels(y)

        # first_pass = not hasattr(self, "coefs_") or (
        #     not self.warm_start and not incremental
        # )

        self.X_ = X.T
        if self.classes_.size > 2:
            self._label_binarizer = LabelBinarizer()
            self.y_ = self._label_binarizer.fit_transform(y).T

        else:
            self.y_ = y.reshape(1, y.shape[0])

        if self.verbose:
            print(f"X shape: {self.X_.shape}")
            print(f"y shape: {self.y_.shape}")

        if self.random_state is not None:
            np.random.seed(self.random_state)

        self._initialize_weights_and_bias()

        train_loss = []
        train_acc = []

        for i in range(self.n_iter):
            activations = self._forward_propagation(self.X_)
            if self.verbose and i == 0:
                for i, A in enumerate(activations, start=1):
                    print(f"A{i} shape: {A.shape}")

            gradients = self._back_propagation(activations)
            self._update(gradients)

            if return_fit_report:
                train_loss.append(log_loss(activations[-1], self.y_))
                train_acc.append(self.score(X, y))

        if return_fit_report:
            return train_loss, train_acc

    def predict(self, X):
        """A reference implementation of a prediction for a classifier.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The input samples.
        Returns
        -------
        y : ndarray, shape (n_samples,)
            The label for each sample is the label of the closest sample
            seen during fit.
        """
        # Check is fit had been called
        check_is_fitted(self, ["X_", "y_"])

        # Input validation
        X = check_array(X).T

        activations = self._forward_propagation(X)
        if self.classes_.size == 2:
            y_pred = activations[-1] >= 0.5
        else:
            if self.verbose:
                print(activations[-1].T[0:10])

            y_pred = self._label_binarizer.inverse_transform(activations[-1].T)

        return y_pred.flatten()

    def predict_proba(self, X):
        """A reference implementation of a prediction for a classifier.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The input samples.
        Returns
        -------
        y : ndarray, shape (n_samples,)
            The label for each sample is the label of the closest sample
            seen during fit.
        """
        # Check is fit had been called
        check_is_fitted(self, ["X_", "y_"])

        # Input validation
        X = check_array(X)

        activations = self._forward_propagation(X)
        if self.classes_.size == 2:
            y_pred_proba = activations[-1]
        else:
            y_pred_proba = activations[-1].T

        return y_pred_proba
